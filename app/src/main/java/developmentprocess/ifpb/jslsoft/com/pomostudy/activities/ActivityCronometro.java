package developmentprocess.ifpb.jslsoft.com.pomostudy.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import developmentprocess.ifpb.jslsoft.com.pomostudy.R;
import developmentprocess.ifpb.jslsoft.com.pomostudy.component.ConfirmDialogHandler;
import developmentprocess.ifpb.jslsoft.com.pomostudy.component.ProgressIndicator;
import developmentprocess.ifpb.jslsoft.com.pomostudy.component.TextViewOutline;
import developmentprocess.ifpb.jslsoft.com.pomostudy.config.ChangePhoneMode;
import developmentprocess.ifpb.jslsoft.com.pomostudy.config.MyNotifications;
import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.Fachada;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Atividade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.resource.Colors;
import developmentprocess.ifpb.jslsoft.com.pomostudy.services.CountDownBrodaCastService;

/**
 * Esta classe permite o controle e a visualização do contador regressivo para
 * que seja possivel a contagem dos pomodoros. Nela é informado se a contagem
 * é um pomodoro normal de 25 min, descanso de 5 min ou descanso longo de 15 min
 * e enviado ao service
 * {@link developmentprocess.ifpb.jslsoft.com.pomostudy.services.CountDownBrodaCastService}. <p/>
 *  <p/>
 * {@value #TIMECOUNTDOWN} registro da activity para enviar infomações do controlador por intent. <p/>
 * {@value #POMOTIME} tempo em milisegundos equivalente a 25 min. <p/>
 * {@value #POMOPAUSE} tempo em milisegundos equivalente a 5 min. <p/>
 * {@value #POMOLONGPAUSE} tempo em milisegundos equivalente a 15 min. <p/>
 * <p/>
 * @author leonardo.soares.ws@gmail.com, Sidicley Wilker <p/>
 * @see developmentprocess.ifpb.jslsoft.com.pomostudy.services.CountDownBrodaCastService .
 */
public class ActivityCronometro extends Activity {

    public static final String TIMECOUNTDOWN = "developmentprocess.ifpb.jslsoft.com.pomostudy.activities.init_time";
    public static final long POMOTIME = 1500000;
    public static final long POMOPAUSE = 300000;
    public static final long POMOLONGPAUSE = 900000;

    private TextViewOutline txtViewHoras;
    private ProgressIndicator progressIndicator;
    private Button btnPular, btnDescanco, btnReiniciar1,
            btnValidar, btnReiniciar2, btnFinalizar,
            btnFechar1, btnFechar2;
    private ConfirmDialogHandler cdh;

    private LinearLayout lLayoutPomo, lLayoutValidate, lLayoutCancel;

    // Cria um objeto Broadcast para recuperar infromações do service.
    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGUI(intent);
        }
    };

    private long totalTime = 0;
    private int validPomo = 0; // possibilidade de resgatar valor da atividade se for possivel recomeçar uma atividade.
    private boolean isPomodoro = false; // indica se é um pomodoro ou um descanso
    private boolean isLongBreak = false;
    private Atividade atividade;
    private Fachada fachada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_conometro_pomodoro);
        fachada = new Fachada(this);

        this.atividade = fachada.getAtividade(getIntent().getIntExtra("EXTRA_ATIVIDADE_KEY", 0));

        // liga as luzes do dispositivo
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // inicia os componentes do layout.
        initComponent();

        // inicia o pomodoro quando é criada a activity.
        startPomodoro(new Long(50000));
        isPomodoro = true;
    }

    /* Este método inicia os componentes da activity. */
    private void initComponent() {
        txtViewHoras = (TextViewOutline) findViewById(R.id.txtViewHoras);

        // Botões
        btnPular = (Button) findViewById(R.id.btnSeguir);
        btnDescanco = (Button) findViewById(R.id.btnDescanco);
        btnValidar = (Button) findViewById(R.id.btnValidar);
        btnReiniciar1 = (Button) findViewById(R.id.btnReiniciar1);
        btnReiniciar2 = (Button) findViewById(R.id.btnReiniciar2);
        btnFechar1 = (Button) findViewById(R.id.btnFechar1);
        btnFechar2 = (Button) findViewById(R.id.btnFechar2);
        btnFinalizar = (Button) findViewById(R.id.btnFinalizar);

        // Layouts
        lLayoutPomo = (LinearLayout) findViewById(R.id.lLayoutPomo);
        lLayoutValidate = (LinearLayout) findViewById(R.id.lLayoutValidate);
        lLayoutCancel = (LinearLayout) findViewById(R.id.lLayoutCancel);

        progressIndicator = (ProgressIndicator) findViewById(R.id.determinate_progressbar);

        setComponents();
    }

    /* Este método configura os componentes iniciados */
    private void setComponents() {

        validPomo = 0;

        // define a cor superior da barra do contador.
        progressIndicator.setForegroundColor(
                Color.parseColor(Colors.DARKGREEN.getValue()));

        // define a cor inferior da barra do contador.
        progressIndicator.setBackgroundColor(
                Color.parseColor(Colors.WHITEGREEN.getValue()));

        // muda os botões para invisivel
        lLayoutPomo.setVisibility(View.INVISIBLE);
        lLayoutCancel.setVisibility(View.VISIBLE);
        lLayoutValidate.setVisibility(View.INVISIBLE);


        // ação do botão novo pomo.
        btnPular.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                startPomodoro(new Long(5000));
                isPomodoro = true;
            }
        });

        // ação do botão descanço
        btnDescanco.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(isLongBreak){
                    startPomodoro(new Long(15000));
                } else {
                    startPomodoro(new Long(5000));
                }
                isPomodoro = false;
                isLongBreak = false;

                // Ativar o wi-fi no descando
                ChangePhoneMode.ativarWifi(ActivityCronometro.this);
            }
        });

        // ação do botão reiniciar
        btnReiniciar1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                restartPomodoro(totalTime);
            }
        });
        btnReiniciar2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                restartPomodoro(totalTime);
            }
        });

        // ação do botão fechar
        btnFechar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // chama o alerta para fechar o pomodoro e parar a atividade
                callExitAlert();
            }
        });
        btnFechar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // chama o alerta para fechar o pomodoro e parar a atividade
                callExitAlert();
            }
        });

        // ação do botão validar pomodoro
        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validatePomodoro();
            }
        });


        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // chama o alert para finalizar o pomodoro e concluir a atividade.
                callFinalizeAlert();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // verifica se o contador no service acabou.
        if (CountDownBrodaCastService.finished) {
            finishPomodoro();
        }

        // registra o receiver
        registerReceiver(br, new IntentFilter(CountDownBrodaCastService.COUNTDOWN_BR));
    }

    @Override
    protected void onStop() {

        // "desregistra" o receiver
        unregisterReceiver(br);

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(ActivityCronometro.this,
                CountDownBrodaCastService.class));
        cancelNotification();
        super.onDestroy();
    }

    /**
     * Método que é invocado ao apertar o botaão "back" do aparelho,
     * ele faz com que seja chamado uma alert para que o usuário confrime
     * se ele quer parar o pomodoro ou não.
     *
     * @see developmentprocess.ifpb.jslsoft.com.pomostudy.component.ConfirmDialogHandler
     */
    public void onBackPressed() {
        // chama o alerta para parar a atividade
        callExitAlert();
    }

    /* Método que inicia o pomodoro */
    private void startPomodoro(Long typeTime) {
        try {
            // tenta para o service do contador
            stopService(new Intent(this, CountDownBrodaCastService.class));
            this.totalTime = typeTime;
        } catch (Exception ignored) {
        }

        // chama método para desligar o wi-fi para que o usuário não seja interonpido
        ChangePhoneMode.desativarWifi(ActivityCronometro.this);

        // muda os botões para invisível
        lLayoutPomo.setVisibility(View.INVISIBLE);
        lLayoutValidate.setVisibility(View.INVISIBLE);
        lLayoutCancel.setVisibility(View.VISIBLE);

        Intent countIntent = new Intent(this, CountDownBrodaCastService.class);

        // coloca as informações do tempo total no intent
        countIntent.putExtra(TIMECOUNTDOWN, typeTime);

        // inicia o service do contador
        startService(countIntent);
    }

    /* Método que finaliza o pomodoro */
    private void finishPomodoro() {

        // cancela notificação
        cancelNotification();

        // retorna o botão de descanso ao normal caso seja um longbreak
        btnDescanco.setText(R.string.btn_descasar);

        // muda os botões para visível
        if(isPomodoro) {
            btnDescanco.setEnabled(true);
            lLayoutCancel.setVisibility(View.INVISIBLE);
            lLayoutValidate.setVisibility(View.VISIBLE);
        } else {
            lLayoutCancel.setVisibility(View.INVISIBLE);
            lLayoutPomo.setVisibility(View.VISIBLE);
            btnDescanco.setEnabled(false);
        }

        // muda o texto do contador
        txtViewHoras.setText("Fim");

        // zera a barra do contador
        progressIndicator.setValue(0);

        // muda a variavel finish do service para false e assim podendo
        // iniciar outro contador sem que ele zere novamente ao resumir o layout
        CountDownBrodaCastService.finished = false;
    }

    /* Método que valida o pomodoro */
    public void validatePomodoro() {
        // Persiste um novo pomodoro
        fachada.validarPomodoro(this.atividade);

        //adiciona o contador
        validPomo++;

        if((validPomo % 4) == 0) {
            isLongBreak = true;
            btnDescanco.setText("vai um café?");
        }

        lLayoutValidate.setVisibility(View.INVISIBLE);
        lLayoutPomo.setVisibility(View.VISIBLE);

        Log.i("Pomodoro", validPomo+"");
    }

    /* Metodo que reinicia a contagem */
    private void restartPomodoro(long time) {

        // para o service
        stopService(new Intent(ActivityCronometro.this,
                CountDownBrodaCastService.class));

        // inicia o service
        startPomodoro(time);
    }

    private void finalizeAtividade() {
        fachada.concluirAtividade(this.atividade);
        Toast.makeText(this, "Atiividade " + this.atividade.getNome()
                + " foi finalizada!", Toast.LENGTH_SHORT).show();
    }

    /**
     * Metodo da primeira opção do alert que para fechar atividade.
     * finalizeAtividade chave que indica se é para finalizar a atividade ou não
     */
    private Runnable stopAtividade(final boolean finalizeAtividade) {
        return new Runnable() {
            @Override
            public void run() {

                if(finalizeAtividade) {
                    finalizeAtividade();
                }

                stopService(new Intent(ActivityCronometro.this,
                        CountDownBrodaCastService.class));
                cancelNotification();

                startActivity(new Intent(ActivityCronometro.this, AtividadeListActivity.class));
                finish();

                // Chama os método de ativar o wi-fi
                ChangePhoneMode.ativarWifi(ActivityCronometro.this);
            }
        };
    }

    /* Metodo da segunda opção do alert que não para atividade */
    private Runnable continueAtividade() {
        return new Runnable() {
            @Override
            public void run() {
                // TODO Não acontece nada.
            }
        };
    }

    /* Metodo que atualiza a interface grafica a cada "tick" do contador */
    public void updateGUI(Intent intent) {
        if (intent.getExtras() != null) {

            boolean fininsh = intent.getBooleanExtra("finish", false);
            long millisToFormatTimer = intent.getLongExtra("countdown", 0);
            float porcentagem = millisToFormatTimer * 100 / totalTime;

            Log.i("Foi?", "" + porcentagem + "  " + totalTime);

            progressIndicator.setValue(new Float(0.010 * porcentagem));
            String hms = millisFormatToString(millisToFormatTimer);
            txtViewHoras.setText(hms);

            if (fininsh) {
                finishPomodoro();
                try {
                    cdh.getDialog().dismiss();
                } catch (Exception ignored) {}
            }
        }
    }

    /* Transforma os milisegundos em formato de texto */
    private String millisFormatToString(long millis) {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis));
        long secounds = TimeUnit.MILLISECONDS.toSeconds(millis)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));

        return String.format("%02d:%02d", minutes, secounds);
    }

    private void callExitAlert() {
        cdh = new ConfirmDialogHandler(this, "Alerta", "Deseja realmente parar a atividade?",
                "Sim", "Não", stopAtividade(false), continueAtividade());
    }

    private void callFinalizeAlert() {

        cdh = new ConfirmDialogHandler(this, "Alerta", "Deseja concluir essa atividade?",
                "Sim", "Não", stopAtividade(true), continueAtividade());
    }

    /* Cancela notificação */
    private void cancelNotification() {
        // cancela a notificação
        NotificationManager nm =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(MyNotifications.NOTIFICATION_KEY);
    }
}
