package developmentprocess.ifpb.jslsoft.com.pomostudy.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import developmentprocess.ifpb.jslsoft.com.pomostudy.R;
import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.Fachada;
import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.IPomoStudyFacade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.dialog.AtividadeCadastroDialog;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Atividade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Categoria;

/**
 * Activity da lista de atividades.
 *
 * Created by João Nunes on 31/01/2015.
 */
public class AtividadeListActivity extends ListActivity implements AdapterView.OnItemClickListener,
        AtividadeCadastroDialog.NoticeDialogListener {

    private List<Map<String, Object>> atividades;
    private ActionMode mActionMode;
    private static Context mContext;
    private SimpleAdapter adapter;
    private Atividade atividadeSelecionada;
    private IPomoStudyFacade fachada;
    private Activity ref; // Necessária para o AlertDialog Builder
    private Categoria categoriaFiltro;
    private boolean filtradoPorCategoria;
    private boolean mostrarAbertas;
    private boolean mostrarFechadas;
    private boolean filtradoPorStatus;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        ref = this;
        fachada = new Fachada(this);

        getActionBar().setTitle("Atividades");

        // Alguns métodos precisam deste contexto
        mContext = getApplicationContext();

        if(getIntent().hasExtra("EXTRA_CATEGORIA_KEY")){
            categoriaFiltro = fachada.getCategoria(getIntent().getIntExtra("EXTRA_CATEGORIA_KEY", 0));
            filtradoPorCategoria = true;
        }else{
            filtradoPorCategoria = false;
        }

        // Header da lista de atividades
        View header = View.inflate(this, R.layout.lista_atividade_header, null);

        // Exibe o botão de voltar junto ao nome do app (boa prática)
        if(getActionBar()!=null)
            getActionBar().setDisplayHomeAsUpEnabled(true);

        TextView categoriaNome = (TextView) header.findViewById(R.id.categoriaNome);
        //categoriaNome.setText(getIntent().getExtras().getString("categoria")); // O nome da categoria vem da lista de categorias
        categoriaNome.setText("Atividades");
        //getListView().addHeaderView(header, null, false); // Adiciona o header a listactivity

        mostrarFechadas = false;
        mostrarAbertas = true;

        criarAdapter();

        // Muda o background da listActivity
        getListView().setBackgroundResource(R.drawable.img_back);

        getListView().setOnItemClickListener(this);

        // Exibi o componente actionbar com opção de edição e remoção quando um long click for realizado
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, Object> item = (Map<String, Object>) adapter.getItem(position); // Se colocar header, subtrair em 1
                setAtividadeSelecionada(item.get("atividadeObject"));
                mActionMode = AtividadeListActivity.this.startActionMode(new ActionBarCallBack());
                return true;
            }
        });

    }

    public void criarAdapter(){
        // Array(s) contendo chaves e ids dos componentes
        String[] de = {"atividadeNome", "atividadeIcone", "atividadeStats", "barraProgresso"};
        int[] para = {R.id.atividadeNome, R.id.atividadeIcone, R.id.atividadeStats, R.id.barraProgresso};

        adapter = new SimpleAdapter(this, listarTarefas(), R.layout.lista_atividade, de, para);
        adapter.setViewBinder(new AtividadeViewBinder());
        setListAdapter(adapter);
    }

    /**
     * Preenche o menu de itens para uso no action bar.
     *
     * @param menu Menu que será preenchido
     * @return Boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.atividade_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Lida com a seleção de opções no menu.
     *
     * @param item Item selecionado
     * @return Boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toggleStatus:
                filtraPorStatus();
                criarAdapter();
                return true;
            case R.id.refresh:
                filtradoPorCategoria = false;
                criarAdapter();
                return true;
            case R.id.categorias:
                abrirCategorias();
                return true;
            case R.id.cadastrar:
                showCadAtividadeDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void abrirCategorias() {
        startActivity(new Intent(this, CategoriaListActivity.class));
    }

    public void showCadAtividadeDialog(){
        AtividadeCadastroDialog dialog = new AtividadeCadastroDialog();
        dialog.show(getFragmentManager(), "CadAtividadeDialog");
    }

    /**
     * Fornece a lista de itens da lista de atividades.
     * Está pronto para colocar persistência.
     *
     * @return Lista de mapas, que representam cada, um item na lista.
     */
    private List<Map<String, Object>> listarTarefas() {

        List<Atividade> atividadeItemList;
        if(filtradoPorCategoria){
            atividadeItemList = fachada.listarAtividadesDeCategoria(categoriaFiltro);
        }else if(filtradoPorStatus){
            if(mostrarFechadas) {
                atividadeItemList = fachada.listarAtividadesFechadas();
                getActionBar().setTitle("Atividades Finalizadas");
            }else{
                atividadeItemList = fachada.listarAtividadesAbertas();
                getActionBar().setTitle("Atividades");
            }
        }else{
            atividadeItemList = fachada.listarAtividadesAbertas(); //TODO
        }

        // Caso seja chamado um filtro por status, esse valor muda
        // Deve ser refatorado
        filtradoPorStatus = false;

        // Ordena pela deadline em ordem crescente
        Collections.sort(atividadeItemList, new Comparator<Atividade>() {
            @Override
            public int compare(Atividade a1, Atividade a2) {
                return a1.getDeadline().compareTo(a2.getDeadline());
            }
        });

        atividades = new ArrayList<Map<String, Object>>();
        Map<String, Object> item;
        for(Atividade a : atividadeItemList){
            item = new HashMap<>();
            item.put("atividadeNome", a.getNome());
            item.put("atividadeStats", a.getPomodoroRealizado() + "/" + a.getPomodoroPrevisto());
            if(a.getPomodoroPrevisto() < a.getPomodoroRealizado()){
                item.put("barraProgresso", new Integer[]{a.getPomodoroRealizado(), a.getPomodoroRealizado(), a.getPomodoroPrevisto()});
            }else{
                item.put("barraProgresso", new Integer[]{a.getPomodoroPrevisto(), a.getPomodoroRealizado(), a.getPomodoroRealizado()});
            }
            item.put("atividadeObject", a);
            atividades.add(item);
        }

        vincularIcones(atividades);
        return atividades;
    }

    /**
     * Vincula o ícone a atividade, a partir da letra inicial do nome da atividade.
     *
     * @param dados Uma lista de mapas (cada mapa representa uma atividade) que
     *              será alterado com a inclusão de um ícone.
     */
    private void vincularIcones(List<Map<String, Object>> dados) {
        for(Map<String, Object> item : dados){
            char primeiraLetra = ((String) item.get("atividadeNome")).toLowerCase().charAt(0);
            item.put("atividadeIcone", getResources().getIdentifier("letter_" + primeiraLetra, "drawable", getPackageName()));
        }
    }

    /**
     * Inica o pomodoro a partir da atividade selecionada.
     *
     * @param parent AdapterView
     * @param view View
     * @param position Indica a posição do item selecionado
     * @param id Indentificação do item selecionado
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Map<String, Object> item = (Map<String, Object>) adapter.getItem(position); // Se colocar header, subtrair em 1
        setAtividadeSelecionada(item.get("atividadeObject"));
        // Impede que o pomodoro inicie para uma atividade concluida
        if(getAtividadeSelecionada().getConcluida() == 0) {
            Intent pomodoro = new Intent(this, ActivityCronometro.class);
            pomodoro.putExtra("EXTRA_ATIVIDADE_KEY", getAtividadeSelecionada().getId());
            startActivity(pomodoro);

            if(mActionMode != null)
                mActionMode.finish();

            // fecha a activity
            finish();
        }
    }

    /**
     * Classe interna para a barra de ação com o botão de editar e deletar.
     */
    private class ActionBarCallBack implements ActionMode.Callback {
        // Prenche a actionbar com os itens a partir do xml
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_menu_ativ, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            mode.setTitle("Opções");
            return false;
        }

        /**
         * Lida com as seleções realizadas na actionbar de opções.
         *
         * @param mode ActionMode
         * @param item Item selecionado
         * @return Boolean
         */
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.finalizarAtividade:
                    finalizaAtividade();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.editar:
                    AtividadeCadastroDialog dialog = new AtividadeCadastroDialog();
                    dialog.setArguments(getBundle());
                    dialog.show(getFragmentManager(), "CadAtividadeDialog");
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.deletar:
                    confirmarRemocao();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        private Bundle getBundle(){
            Bundle args = new Bundle();
            args.putInt("key", atividadeSelecionada.getId());
            return args;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mode = null;
        }

    }

    /**
     * Muda o status de uma atividade para fechada caso esteja aberta.
     * Caso a atividade já esteja finalizada, o inverso acontece (desfaz).
     */
    private void finalizaAtividade() {
        fachada.concluirAtividade(atividadeSelecionada);
        criarAdapter();
    }

    /**
     * Exibe um alerta na tela para que o usuário confirme a remoção da atividade.
     */
    private void confirmarRemocao() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ref);
        builder.setMessage("Deseja realmente remover a atividade?");
        builder.setPositiveButton("Remover", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                removeAtividade();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
           public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    /**
     * Classe interna necessária para fazer o bind da barra de progresso de pomodoros.
     */
    private class AtividadeViewBinder implements SimpleAdapter.ViewBinder{

        @Override
        public boolean setViewValue(View view, Object data, String textRepresentation){
            if (view.getId() == R.id.barraProgresso) {
                Integer valores[] = (Integer[]) data;
                ProgressBar progressBar = (ProgressBar) view;
                progressBar.setMax(valores[0].intValue());
                progressBar.setSecondaryProgress(valores[1].intValue());
                progressBar.setProgress(valores[2].intValue());
                return true;
            }
            return false;
        }

    }

    /**
     * Recupera esse contexto.
     *
     * @return Contexto desta ListActivity para ser utilizado em alguns métodos.
     */
    public static Context getContext() {
        return mContext;
    }

    /**
     * Método responsável por atualizar a lista quando uma atividade é cadastrada.
     *
     * @param dialog Dialog Fragment de cadastro de atividade.
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        criarAdapter();
    }

    /**
     * Remove a atividade selecionada na lista de atividades e atualiza a lista.
     */
    private void removeAtividade(){
        fachada.removeAtividade(atividadeSelecionada);
        criarAdapter();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    /**
     * Recupera a atividade referente ao item selecionado na lista de atividades.
     *
     * @return Atividade
     */
    public Atividade getAtividadeSelecionada() {
        return atividadeSelecionada;
    }

    /**
     * Sempre que quaisquer tipo de click em um item da lista for realizado,
     * a atividade referente deve ser armazenada para processamento futuro.
     *
     * @param atividadeSelecionada Atividade selecionada
     */
    public void setAtividadeSelecionada(Object atividadeSelecionada) {
        this.atividadeSelecionada = (Atividade) atividadeSelecionada;
    }

    /**
     * Funcionalidade temporária que auxilia na realização de filtros.
     * Deve ser refatorada.
     */
    private void filtraPorStatus() {
        filtradoPorStatus = true;
        if(mostrarAbertas){
            mostrarAbertas = false;
            mostrarFechadas = true;
        }else{
            mostrarAbertas = true;
            mostrarFechadas = false;
        }
    }
}
