package developmentprocess.ifpb.jslsoft.com.pomostudy.activities;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import developmentprocess.ifpb.jslsoft.com.pomostudy.R;
import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.Fachada;
import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.IPomoStudyFacade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.dialog.CategoriaCadastroDialog;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Categoria;

/**
 * ListActivity de categorias.
 *
 * Created by João Nunes on 30/01/2015.
 */
public class CategoriaListActivity extends ListActivity implements OnItemClickListener, CategoriaCadastroDialog.NoticeDialogListener{

    private List<Map<String, Object>> categorias;
    private ActionMode mActionMode;
    private static Context mContext;
    private IPomoStudyFacade fachada;
    private Categoria categoriaSelecionada;
    private SimpleAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();

        fachada = new Fachada(this);

        getActionBar().setTitle("Categorias");

        // Exibe o botão de voltar junto ao nome do app (boa prática)
        if(getActionBar()!=null)
            getActionBar().setDisplayHomeAsUpEnabled(true);

        // Header da lista de atividades
        //View view = View.inflate(this, R.layout.lista_categoria_header, null);
        //getListView().addHeaderView(view, null, false);

        criarAdapter();

        // Muda o background da listActivity
        getListView().setBackgroundResource(R.drawable.img_back);

        // Adiciona ouvinte para click simples
        getListView().setOnItemClickListener(this);

        // Adicona ouvinte para long click
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, Object> item = (Map<String, Object>) adapter.getItem(position); // Se colocar header, subtrair em 1
                setCategoriaSelecionada(item.get("categoriaObject"));
                mActionMode = CategoriaListActivity.this.startActionMode(new ActionBarCallBack());
                return true;
            }
        });

    }

    /**
     * Preenche o menu com itens contidos no xml para uso na action bar
     * @param menu
     * @return Boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.categoria_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Lida com as ações de seleção de item na action bar.
     * @param item Item de menu
     * @return Boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.cadastrar:
                showCadCategoriaDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showCadCategoriaDialog() {
        CategoriaCadastroDialog dialog = new CategoriaCadastroDialog();
        dialog.show(getFragmentManager(), "CadCategoriaDialog");
    }

    /**
     * Cria o menu a partir do modelo definido no xml.
     *
     * @param menu Menu
     * @param v View
     * @param menuInfo Informações do menu
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    /**
     * Dados que servem para preencher a lista.
     * Será alterado quando a persistência for implantada.
     *
     * @return Lista de itens (mapas) para preencher a ListActivity
     */
    private List<Map<String, Object>> listarCategorias() {
        List<Categoria> categoriaItemList = fachada.listarCategorias();
        categorias = new ArrayList<Map<String, Object>>();
        Map<String, Object> item;
        for(Categoria c : categoriaItemList){
            item = new HashMap<>();
            item.put("categoriaNome", c.getNome());
            item.put("categoriaObject", c);
            categorias.add(item);
        }

        vincularIcones(categorias);
        return categorias;
    }

    /**
     * Vincula os icones a cada item de categoria, a partir de sua letra inicial.
     *
     * @param dados Lista de itens que serão alteradas
     */
    private void vincularIcones(List<Map<String, Object>> dados) {
        for(Map<String, Object> item : dados){
            char primeiraLetra = ((String) item.get("categoriaNome")).toLowerCase().charAt(0);
            item.put("categoriaIcone", getResources().getIdentifier("letter_" + primeiraLetra, "drawable", getPackageName()));
        }
    }

    /**
     * Lida com os clicks efetuados nos itens da lista de categorias.
     *
     * @param parent AdapterView
     * @param view View
     * @param position Posição da view na ListActivity
     * @param id Identificação
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Map<String, Object> item = (Map<String, Object>) adapter.getItem(position); // Se colocar header, subtrair em 1
        setCategoriaSelecionada(item.get("categoriaObject"));
        Intent listaDeAtividades = new Intent(this, AtividadeListActivity.class);
        listaDeAtividades.putExtra("EXTRA_CATEGORIA_KEY", getCategoriaSelecionada().getId());
        startActivity(listaDeAtividades);
        if(mActionMode != null)
            mActionMode.finish();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        criarAdapter();
    }

    /**
     * Classe responsável por preencher o menu contextual de opções a partir do xml.
     */
    private class ActionBarCallBack implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        /**
         * Lida com a seleção de itens no menu contextual (editar e remover).
         *
         * @param mode Barra
         * @param item Item
         * @return
         */
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.editar:
                    CategoriaCadastroDialog dialog = new CategoriaCadastroDialog();
                    dialog.setArguments(getBundle());
                    dialog.show(getFragmentManager(), "CadAtividadeDialog");
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.deletar:
                    confirmarRemocao();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        private Bundle getBundle(){
            Bundle args = new Bundle();
            args.putInt("key", categoriaSelecionada.getId());
            return args;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mode = null;
        }

    }

    private void removeCategoria() {
        fachada.removeCategoria(categoriaSelecionada);
        criarAdapter();
    }

    public void criarAdapter(){
        // Array(s) contendo chaves e ids dos componentes da lista
        String[] de = {"categoriaIcone", "categoriaNome"};
        int[] para = {R.id.categoriaIcone, R.id.categoriaNome};

        // Cria o Adapter que preenche a lista de categorias
        adapter = new SimpleAdapter(this, listarCategorias(), R.layout.lista_categoria, de, para);
        setListAdapter(adapter);
    }

    /**
     * Retorna o contexto dessa ListActivity para uso em alguns métodos internos.
     * @return Contexto da ListActivity
     */
    public static Context getContext() {
        return mContext;
    }

    public Categoria getCategoriaSelecionada() {
        return categoriaSelecionada;
    }

    public void setCategoriaSelecionada(Object categoriaSelecionada) {
        this.categoriaSelecionada = (Categoria) categoriaSelecionada;
    }

    private void confirmarRemocao() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("As atividades da categoria também serão removidas. Confirmar remoção?");
        builder.setPositiveButton("Remover", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                removeCategoria();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
