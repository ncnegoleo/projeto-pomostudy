package developmentprocess.ifpb.jslsoft.com.pomostudy.component;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Esta classe serve como ferramenta para a criação de um "Confrim Dialog"
 * onde chamará um diálogo apresentando uma pergunta e duas respostas que para
 * cada uma ocorrerá uma ação.
 *
 * @author leonardo.soares.ws@gmail.com
 */
public class ConfirmDialogHandler {

    // indica os procedimentos que ocoreão nas duas respostas
    private Runnable res_true = null;
    private Runnable res_false = null;

    private AlertDialog dlg;

    /**
     * Construtor, que configura cria e mostra o dialogo e seus procedimentos.
     *
     * @param act activity no qual o dialogo vai ser gerado.
     * @param title titulo do alerta
     * @param mess mensagem do alerta
     * @param cancelBtnTxt texto do botão que cancela
     * @param confirmBtnTxt texto do botão que confirma
     * @param aProced procedimento a
     * @param bProced procedimento b
     */
    public ConfirmDialogHandler(Activity act, String title, String mess, String cancelBtnTxt,
                                String confirmBtnTxt, Runnable aProced, Runnable bProced) {
        res_true = bProced;
        res_false = aProced;

        // configura e cria o dialogo
        dlg = new AlertDialog.Builder(act)
                .setTitle(title) // titulo
                .setMessage(mess) // mensagem
                .setPositiveButton(confirmBtnTxt, new DialogInterface.OnClickListener() {
                    // procedimento a
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        res_true.run();
                    }
                })
                .setNegativeButton(cancelBtnTxt, new DialogInterface.OnClickListener() {
                    // procedimento b
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        res_false.run();
                    }
                }).create(); // cria o alerta

        // mostra o dialogo
        dlg.show();
    }

    /**
     * Retorna o dialogo criado.
     *
     * @return o dialogo criado.
     */
    public AlertDialog getDialog() {
        return dlg;
    }
}