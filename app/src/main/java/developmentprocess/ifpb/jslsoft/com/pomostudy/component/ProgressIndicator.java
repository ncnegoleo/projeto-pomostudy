package developmentprocess.ifpb.jslsoft.com.pomostudy.component;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.util.AttributeSet;
import android.view.View;

/**
 * Esta classe é usada para mostrar um determinado indicador de progresso
 * em um grafico. Dois tipos de idicadores são suportados "roda circular"
 * e "torta"
 * @author unknow
 */
public class ProgressIndicator extends View {

    private final RectF mRect = new RectF();
    private final RectF mRectInner = new RectF();
    private final Paint mPaintForeground = new Paint();
    private final Paint mPaintBackground = new Paint();
    private final Paint mPaintErase = new Paint();
    private static final Xfermode PORTER_DUFF_CLEAR = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
    private int mColorForeground = Color.WHITE;
    private int mColorBackground = Color.BLACK;
    private float mValue;
    private boolean mPieStyle;


    // O valor que retorna o indicador personalizado têm aproximadamente
    // o mesmo temanho que o ProgressBar inbutido Unit: dp
    private static final float PADDING = 4;
    private float mPadding;
    private Bitmap mBitmap;


    // Valor que torna o nosso indicador personalizado elaborado têm
    // aproximadamente o mesmo hickness como o indicador embutido ProgressBar.
    // Expressa como a razão entre os raios interno e externo
    private static final float INNER_RADIUS_RATIO = 0.84f;

    /**
     * Método construtor.
     *
     * @param context contexto que está executando, através do qual se pode acessar os recursos.
     */
    public ProgressIndicator(Context context) {
        this(context, null);
    }

    /**
     * Método que muda a barra do indicador, informando o progresso.
     *
     * @param context contexto que está executando, através do qual se pode acessar os recursos.
     * @param attrs os atributos da tag XML que está na view.
     */
    public ProgressIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);

        Resources r = context.getResources();
        float scale = r.getDisplayMetrics().density;

        mPadding = scale * PADDING ;
        mPaintForeground.setColor(mColorForeground);
        mPaintForeground.setAntiAlias(true);
        mPaintBackground.setColor(mColorBackground);
        mPaintBackground.setAntiAlias(true);
        mPaintErase.setXfermode(PORTER_DUFF_CLEAR);
        mPaintErase.setAntiAlias(true);
    }

    /**
     * Escolhe o tipo de indicador que podem ser "wheel" and "pie"
     * @param pieStyle se <tt>True</tt> retorna o indicador "wheel" se
     *                 se não retorna o indicador "pie"
     */
    public void setPieStyle(boolean pieStyle) {
        if (mPieStyle == pieStyle) {
            return;
        }
        mPieStyle = pieStyle;
        updateBitmap();
    }

    /**
     * Retorna o indicador corrente.
     * @return <tt>True</tt> se o indicador for um "pie" style
     */
    public boolean getIsPieStyle() {
        return mPieStyle;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(mBitmap, getWidth() / 2 - mBitmap.getWidth() / 2,
                getHeight() / 2 - mBitmap.getHeight() / 2, null);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {

        float bitmapWidth = w - 2 * mPadding;
        float bitmapHeight = h - 2 * mPadding;
        float radius = Math.min(bitmapWidth / 2, bitmapHeight / 2);

        mRect.set(0, 0, bitmapWidth, bitmapHeight);
        radius *= INNER_RADIUS_RATIO;
        mRectInner.set(bitmapWidth / 2f - radius, bitmapHeight / 2f - radius, bitmapWidth / 2f + radius, bitmapHeight / 2f + radius);

        updateBitmap();
    }

    /**
     * Muda a cor do indicador superior da barra de progresso.
     * @param color representa a cor a ser mudada.
     */
    public void setForegroundColor(int color) {
        this.mColorForeground = color;
        mPaintForeground.setColor(color);
        invalidate();
    }

    /**
     * Muda a cor do indicador inferior da barra de progresso.
     * @param color represnta a cor a ser mudada.
     */
    public void setBackgroundColor(int color) {
        this.mColorBackground = color;
        mPaintBackground.setColor(color);
        invalidate();
    }

    /**
     * Método para a sincrinização do Thread
     * @param value Um numero entre 0 e 1
     */
    public synchronized void setValue(float value) {
        mValue = value;
        updateBitmap();
    }

    private void updateBitmap() {
        if (mRect == null || mRect.width() == 0) {
            return;
        }
        mBitmap = Bitmap.createBitmap((int) mRect.width(), (int) mRect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mBitmap);
        canvas.drawArc(mRect, -90, 360, true, mPaintBackground);
        if (mValue < 0.01f) {
            canvas.drawLine(mRect.width() / 2, mRect.height() / 2, mRect.width() / 2, 0, mPaintForeground);
        }
        float angle = mValue * 360;
        canvas.drawArc(mRect, -90, angle, true, mPaintForeground);
        if (!mPieStyle) {
            canvas.drawArc(mRectInner, -90, 360, true, mPaintErase);
        }
        postInvalidate();
    }
}