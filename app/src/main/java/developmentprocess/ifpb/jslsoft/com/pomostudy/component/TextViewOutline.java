package developmentprocess.ifpb.jslsoft.com.pomostudy.component;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Esta classe serve para modificar o componente TextView para que o
 * mesmo fique com uma borda contornando o texto
 * @author unknow
 */
public class TextViewOutline extends TextView {


    /**
     * Contrutor, as definições e metodos chamados ficaram por reponsabilidade da classe pai;
     *
     * @param context contexto que está executando, através do qual se pode acessar os recursos.
     */
    public TextViewOutline(Context context) {
        super(context);
    }

    /**
     * Contrutor, as definições e metodos chamados ficaram por reponsabilidade da classe pai;
     *
     * @param context contexto que está executando, através do qual se pode acessar os recursos.
     * @param attrs os atributos da tag XML que está na view.
     */
    public TextViewOutline(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Contrutor, as definições e metodos chamados ficaram por reponsabilidade da classe pai;
     *
     * @param context contexto no qual será integrado o TextView
     * @param attrs os atributos da tag XML que está na view.
     * @param defStyleAttr define o estilo dos atributos.
     */
    public TextViewOutline(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Contrutor, as definições e metodos chamados ficaram por reponsabilidade da classe pai;
     *
     * @param context contexto no qual será integrado o TextView
     * @param attrs os atributos da tag XML que está na view.
     * @param defStyleAttr define o estilo dos atributos.
     * @param defStyleRes define o estilo do resoucer.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TextViewOutline(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * Este metodo é modificado para desenhar uma borda ao redor do TextView
     * @param canvas A tela que o View é processado.
     */
    @Override
    public void draw(Canvas canvas) {
        for (int i = 0; i < 5; i++) {
            super.draw(canvas);
        }
    }
}