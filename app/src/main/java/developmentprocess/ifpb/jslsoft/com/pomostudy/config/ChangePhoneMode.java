package developmentprocess.ifpb.jslsoft.com.pomostudy.config;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.net.wifi.WifiManager;

/**
 * Esta classe serve para gerenciar os estados do smartphone pelo app
 * que podem ser ligar/desligar wi-fi e ligar/desligar modo silencioso.
 *
 * @author leonardo.soares.ws@gmail.com, Sidicley Wilker
 */
public class ChangePhoneMode {

    private static  WifiManager wifiManager;

    /**
     * Muda os modo de wi-fi do smartphone para ligado
     *
     * @param context o contexto no qual será chamaodo o método
     */
    public static void ativarWifi(Context context){
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if(wifiManager.isWifiEnabled() == false){
            wifiManager.setWifiEnabled(true);
        }
    }

    /**
     * Muda os modo de wi-fi do smartphone para desligado
     *
     * @param context o contexto no qual será chamaodo o método
     */
    public static void desativarWifi(Context context){
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if(wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(false);
        }
    }

}