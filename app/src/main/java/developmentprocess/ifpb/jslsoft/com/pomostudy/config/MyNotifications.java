package developmentprocess.ifpb.jslsoft.com.pomostudy.config;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import developmentprocess.ifpb.jslsoft.com.pomostudy.R;

/**
 * Esta classe serve para notificar de alguma maneira algum ebendo do app.
 */
public class MyNotifications {

    public static final int NOTIFICATION_KEY = 11123;

    /**
     * Toca um alerta sonoro de Notificação padrão do smartphone
     * @param context contexto no qual a notificação será disparada
     */
    public static void playRingtone(Context context) {

        try {
            Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            MediaPlayer mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Chama uma notificação visual na barra de status do smartphone indicando algo
     *
     * @param context contexto no qual a notificação será disparada
     * @param p intenção pendente que eviará a notificação
     * @param ticker mensagem que aparecerá no momento que a notificação for disparada
     * @param titulo titulo da notificação
     * @param descricao descrição da notificação
     */
    public static void callNotification(Context context, PendingIntent p,
                                        String ticker, String titulo, String descricao) {

        // cria o Manager para a notificação
        NotificationManager nm = (NotificationManager) context
                .getSystemService(context.NOTIFICATION_SERVICE);

        // cria e configura a caixa de notificação
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setTicker(ticker);
        builder.setContentTitle(titulo);
        builder.setContentText(descricao);
        builder.setOngoing(true);
        builder.setAutoCancel(false);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setLargeIcon(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.pomo_large));
        builder.setContentIntent(p);

        // cria a notificação
        Notification n = builder.build();

        // chama a notificação
        nm.notify(NOTIFICATION_KEY, n);
    }

    /**
     * Cria uma chamada de inteção para uma activity no qual será aberta acima
     * de todas as outra aplicações rodadno no smartphone
     * @param context contexto no qual a notificação será disparada
     * @param classe classe da activity que será chamada
     */
    public static void resumeScreen(Context context, Class<?> classe) {
        //Cria a inteção
        Intent intent = new Intent(context.getApplicationContext(), classe);

        // Configura as flags para a intenção
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        // inicia a Atctivity
        context.getApplicationContext().startActivity(intent);
    }
}
