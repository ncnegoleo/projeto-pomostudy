package developmentprocess.ifpb.jslsoft.com.pomostudy.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import developmentprocess.ifpb.jslsoft.com.pomostudy.R;
import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.Fachada;
import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.IPomoStudyFacade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Atividade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Categoria;

/**
 * Dialog Fragment para cadastro e edição de atividades.
 * Created by João on 16/02/2015.
 */
public class AtividadeCadastroDialog extends DialogFragment {

    private ArrayList<String> categoriaNomeList;
    private EditText atividadeNomeField, pomodoroPrevisaoField;
    private AutoCompleteTextView categoriaField;
    private DatePicker deadelineDatePicker;
    private Button salvarButton, cancelarField;
    private NoticeDialogListener mListener;

    private IPomoStudyFacade fachada;
    private Atividade atividade;
    private Categoria categoriaFiltro;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fachada = new Fachada(getActivity());

        // se não for nulo, trata-se da edição de uma atividade
        if(getArguments() != null){
            int key = getArguments().getInt("key");
            atividade = fachada.getAtividade(key); // atividade que será alterada
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        setCancelable(false);
        categoriaNomeList = new ArrayList<>();
        for(Categoria categoria : fachada.listarCategorias()){
            categoriaNomeList.add(categoria.getNome());
        }
        View v = inflater.inflate(R.layout.atividade_dialog, container);
        getDialog().setTitle("Nova Atividade");

        AutoCompleteTextView categorias = (AutoCompleteTextView) v.findViewById(R.id.categoria);
        atividadeNomeField = (EditText) v.findViewById(R.id.atividadeNome);
        pomodoroPrevisaoField = (EditText) v.findViewById(R.id.pomodoroPrevisao);
        categoriaField = (AutoCompleteTextView) v.findViewById(R.id.categoria);
        deadelineDatePicker = (DatePicker) v.findViewById(R.id.deadline);
        salvarButton = (Button) v.findViewById(R.id.salvar);
        cancelarField = (Button) v.findViewById(R.id.cancelar);

        // configura dados do campo de categorias
        ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, categoriaNomeList);
        categorias.setAdapter(adp);

        // preenche campos caso seja edição de atividade
        if(atividade != null)
            preencherCampos();

        // ouvinte do botão cancelarr
        cancelarField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelarRegistro();
            }
        });

        // ouvinte do botão salvarr
        salvarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(camposValidos()) {
                    salvarAtividade();
                }else{
                    alertaCamposInvalidos();
                }
            }
        });

        return v;
    }

    /**
     * Preenche os valores dos campos no caso de edição.
     */
    private void preencherCampos(){
            atividadeNomeField.setText(atividade.getNome());
            pomodoroPrevisaoField.setText(String.valueOf(atividade.getPomodoroPrevisto()));
            categoriaField.setText(atividade.getCategoria().getNome());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(atividade.getDeadline().getTime());

            int ano = cal.get(Calendar.YEAR);
            int mes = cal.get(Calendar.MONTH);
            int dia = cal.get(Calendar.DAY_OF_MONTH);

            deadelineDatePicker.updateDate(ano, mes, dia);
    }

    /**
     * Captura a data de um widget DatePicker.
     * @param datePicker Widget
     * @return Date
     */
    private java.util.Date getDateFromDatePicket(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    /**
     * Salva a atividade no banco de dados.
     * Caso a categoriaa informada ainda não exista, será criada.
     */
    private void salvarAtividade(){
        String nomeDaCategoria = categoriaField.getText().toString();
        String nomeDaAtividade = atividadeNomeField.getText().toString();
        int pomodoroPrevisao = Integer.parseInt(pomodoroPrevisaoField.getText().toString());
        Date deadline = getDateFromDatePicket(deadelineDatePicker);

        if(!categoriaNomeList.contains(nomeDaCategoria))
            fachada.cadastrarCategoria(new Categoria(nomeDaCategoria));

        Categoria categoria = fachada.getCategoriaPorNome(nomeDaCategoria);

        if(atividade == null) {
            atividade = new Atividade(nomeDaAtividade, 0, pomodoroPrevisao, deadline, categoria);
            fachada.cadastrarAtividade(atividade);
            dismiss();
            mListener.onDialogPositiveClick(this);
        }else{
            atividade.setCategoria(categoria);
            atividade.setDeadline(deadline);
            atividade.setNome(nomeDaAtividade);
            atividade.setPomodoroPrevisto(pomodoroPrevisao);
            atividade.setConcluida(0);
            fachada.alterarAtividade(atividade);
            dismiss();
            mListener.onDialogPositiveClick(this);
        }

    }

    /**
     * Cancela o cadastro.
     */
    private void cancelarRegistro(){
        dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Qualquer activity que crie instâncias deste dialog fragment deve
     * implementar esta interface para receber eventos de retorno (event callbacks).
     *
     * The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it.
     */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verifica se a activity que chamou esse dialog fragment implementou a interface de retorno de chamada
        // Verify that the host activity implements the callback interface
        try {
            // Instanciar o NoticeDialogListener para que possamos enviar eventos de retorno para a activity pai
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // A activity não implementa a interface, lança exceção
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    /**
     * Valida campos.
     * @return True -> Dados preenchidos pelo usário são válidos.
     */
    private boolean camposValidos(){
        if(atividadeNomeField.getText().toString().equals("") ||
        pomodoroPrevisaoField.getText().toString().equals("") ||
        categoriaField.getText().toString().equals("")){
            return false;
        }
        return true;
    }

    /**
     * Exibe mensagem de alerta de campos inválidos.
     */
    private void alertaCamposInvalidos() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Preencha todos os campos");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
