package developmentprocess.ifpb.jslsoft.com.pomostudy.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import developmentprocess.ifpb.jslsoft.com.pomostudy.R;
import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.Fachada;
import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.IPomoStudyFacade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Categoria;

/**
 * DialogFragment para cadastro e edição de categorias.
 * Created by João on 16/02/2015.
 */
public class CategoriaCadastroDialog extends DialogFragment {

    private EditText categoriaNomeField;
    private Button salvarButton, cancelarField;
    private NoticeDialogListener mListener;

    private IPomoStudyFacade fachada;
    private Categoria categoria;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fachada = new Fachada(getActivity());

        // se não for nulo, trata-se da edição de uma categoria
        if(getArguments() != null){
            int key = getArguments().getInt("key");
            categoria = fachada.getCategoria(key); // categoria que será alterada
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        setCancelable(false);

        View v = inflater.inflate(R.layout.categoria_dialog, container);
        getDialog().setTitle("Categoria");

        categoriaNomeField = (EditText) v.findViewById(R.id.categoriaNome);
        salvarButton = (Button) v.findViewById(R.id.salvar);
        cancelarField = (Button) v.findViewById(R.id.cancelar);

        // preenche campos caso seja edição da categoria
        if(categoria != null)
            preencherCampos();

        // ouvinte do botão cancelarr
        cancelarField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelarRegistro();
            }
        });

        // ouvinte do botão salvarr
        salvarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(camposValidos()) {
                    salvarCategoria();
                }else{
                    alertaCamposInvalidos();
                }
            }
        });

        return v;
    }

    /**
     * Preenche os valores dos campos no caso de edição.
     */
    private void preencherCampos(){
            categoriaNomeField.setText(categoria.getNome());
    }

    /**
     * Salva a atividade no banco de dados.
     * Caso a categoriaa informada ainda não exista, será criada.
     */
    private void salvarCategoria(){
        String nomeDaCategoria = categoriaNomeField.getText().toString();

        if(categoria == null) {
            categoria = new Categoria(nomeDaCategoria);
            fachada.cadastrarCategoria(categoria);
            dismiss();
            mListener.onDialogPositiveClick(this);
        }else{
            categoria.setNome(nomeDaCategoria);
            fachada.alterarCategoria(categoria);
            dismiss();
            mListener.onDialogPositiveClick(this);
        }

    }

    /**
     * Cancela o cadastro.
     */
    private void cancelarRegistro(){
        dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Qualquer activity que crie instâncias deste dialog fragment deve
     * implementar esta interface para receber eventos de retorno (event callbacks).
     *
     * The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it.
     */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verifica se a activity que chamou esse dialog fragment implementou a interface de retorno de chamada
        // Verify that the host activity implements the callback interface
        try {
            // Instanciar o NoticeDialogListener para que possamos enviar eventos de retorno para a activity pai
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // A activity não implementa a interface, lança exceção
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    /**
     * Verifica se os dados preenchidos são válidos.
     *
     * @return True -> Campos válidos.
     */
    private boolean camposValidos(){
        if(categoriaNomeField.getText().toString().equals(""))
            return false;
        return true;
    }

    /**
     * Exibe uma alerta na tela para que seja informado um nome válido para categoria.
     */
    private void alertaCamposInvalidos() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Informe um nome para a categoria");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
