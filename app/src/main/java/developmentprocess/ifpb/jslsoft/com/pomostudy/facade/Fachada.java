package developmentprocess.ifpb.jslsoft.com.pomostudy.facade;

import android.app.Activity;

import java.util.List;

import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Atividade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Categoria;
import developmentprocess.ifpb.jslsoft.com.pomostudy.persistencia.AtividadeDao;
import developmentprocess.ifpb.jslsoft.com.pomostudy.persistencia.CategoriaDao;
import developmentprocess.ifpb.jslsoft.com.pomostudy.util.AlterarStatusAtividadeComando;
import developmentprocess.ifpb.jslsoft.com.pomostudy.util.IComando;
import developmentprocess.ifpb.jslsoft.com.pomostudy.util.ValidarPomodoroComando;

/**
 * Created by João on 19/02/2015.
 */
public class Fachada implements IPomoStudyFacade {

    private AtividadeDao daoAtividade;
    private CategoriaDao daoCategoria;

    public Fachada(Activity context){
        daoAtividade = new AtividadeDao(context);
        daoCategoria = new CategoriaDao(context);
    }

    @Override
    public void validarPomodoro(Atividade atividade) {
        IComando comando = new ValidarPomodoroComando(atividade, this);
        comando.execute();
    }

    @Override
    public void concluirAtividade(Atividade atividade) {
        IComando comando = new AlterarStatusAtividadeComando(atividade, this);
        comando.execute();
    }

    @Override
    public void cadastrarAtividade(Atividade atividade) {
        daoAtividade.create(atividade);
    }

    @Override
    public void cadastrarCategoria(Categoria categoria) {
        daoCategoria.create(categoria);
    }

    @Override
    public void alterarAtividade(Atividade atividade) {
        daoAtividade.update(atividade);
    }

    @Override
    public void alterarCategoria(Categoria categoria) {
        daoCategoria.update(categoria);
    }

    @Override
    public List<Atividade> listarAtividades() {
        return daoAtividade.buscaTodos();
    }

    @Override
    public List<Atividade> listarAtividadesDeCategoria(Categoria categoria) {
        return daoAtividade.buscarPorCategoria(categoria);
    }

    @Override
    public List<Atividade> listarAtividadesDeCategoria(String categoriaKey) {
        return null;
    }

    @Override
    public List<Atividade> listarAtividadesFechadas() {
        return daoAtividade.buscarPorStatus(true);
    }

    @Override
    public List<Atividade> listarAtividadesAbertas() {
        return daoAtividade.buscarPorStatus(false);
    }

    @Override
    public List<Categoria> listarCategorias() {
        return daoCategoria.buscaTodos();
    }

    @Override
    public Atividade getAtividade(int key) {
        return daoAtividade.buscaPorId(key);
    }

    @Override
    public Categoria getCategoria(int key) {
        return daoCategoria.buscaPorId(key);
    }

    @Override
    public Categoria getCategoriaPorNome(String nome) {
        return daoCategoria.buscarPorNome(nome);
    }

    @Override
    public void removeAtividade(Atividade atividade) {
        daoAtividade.delete(atividade);
    }

    @Override
    public void removeCategoria(Categoria categoria) {
        daoCategoria.delete(categoria);
    }

}
