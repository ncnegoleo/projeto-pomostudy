package developmentprocess.ifpb.jslsoft.com.pomostudy.facade;

import java.util.List;

import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Atividade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Categoria;

/**
 * Facade de acesso as funcionalidades do app.
 *
 * Created by João on 19/02/2015.
 */
public interface IPomoStudyFacade {

    /**
     * Valida um pomodoro.
     * @param atividade Atividade que terá um pomodoro validado.
     */
    public void validarPomodoro(Atividade atividade);

    /**
     * Muda o status de uma atividade para concluida.
     * Se for realizado em uma atividade já concluida, desfaz.
     * @param atividade Atividade que terá o status alterado.
     */
    public void concluirAtividade(Atividade atividade);

    /**
     * Cadastra uma atividade no app.
     * @param atividade Atividade que deve ser cadastrada.
     */
    public void cadastrarAtividade(Atividade atividade);

    /**
     * Cadastra uma categoria no app.
     * @param categoria Categoria que deve ser cadastrada.
     */
    public void cadastrarCategoria(Categoria categoria);

    /**
     * Altera uma atividade no app.
     * @param atividade Atividade que deve ser atualizada com novos dados.
     */
    public void alterarAtividade(Atividade atividade);

    /**
     * Altera uma categoria no app.
     * @param categoria Categoria que deve ser atualizada com novos dados.
     */
    public void alterarCategoria(Categoria categoria);

    /**
     * Lista todas as atividades cadastradas no app.
     * @return Lista de todas atividades cadastradas no app.
     */
    public List<Atividade> listarAtividades();

    /**
     * Lista todas as atividades de uma determinada categoria.
     * @param categoria Objeto Categoria (filtro da consulta).
     * @return Lista de todas as atividades de determinada categoria.
     */
    public List<Atividade> listarAtividadesDeCategoria(Categoria categoria);

    /**
     * Lista todas as atividades de uma determinada categoria.
     * @param categoriaKey Chave da categoria (filtro da consulta).
     * @return Lista de todas as atividades de determinada categoria.
     */
    public List<Atividade> listarAtividadesDeCategoria(String categoriaKey);

    /**
     * Lista todas as atividades fechadas ou concluidas.
     * @return Lista de todas as atividades que já foram concluidas.
     */
    public List<Atividade> listarAtividadesFechadas();

    /**
     * Lista todas as atividades abertas.
     * @return Lista de atividades que ainda não foram concluidas.
     */
    public List<Atividade> listarAtividadesAbertas();

    /**
     * Lista todas as categorias cadastradas no app.
     * @return Lista de todas as categorias.
     */
    public List<Categoria> listarCategorias();

    /**
     * Busca uma atividade no app.
     * @param key Atributo chave de uma atividade.
     * @return Atividade
     */
    public Atividade getAtividade(int key);

    /**
     * Busca uma categoria no app.
     * @param key Atributo chave de uma categoria.
     * @return Categoria
     */
    public Categoria getCategoria(int key);

    /**
     * Busca uma categoria pelo nome no app.
     * @param nome Nome da categoria.
     * @return Categoria
     */
    public Categoria getCategoriaPorNome(String nome);

    /**
     * Remove uma atividade cadastrada no app.
     * @param atividade Objeto Atividade que será removido do app.
     */
    public void removeAtividade(Atividade atividade);

    /**
     * Remove uma categoria cadastrada no app.
     * @param categoria Objeto Categoria que será removido do app.
     */
    public void removeCategoria(Categoria categoria);

}
