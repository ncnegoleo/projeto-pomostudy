package developmentprocess.ifpb.jslsoft.com.pomostudy.model;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by João on 15/02/2015.
 */
public class Atividade {
    private int id;
    private String nome;
    private int pomodoroRealizado;
    private int pomodoroPrevisto;
    private Date deadline;
    private Categoria categoria;
    private int concluida;

    /**
     * Quando a atividade é recuperada do banco de dados.
     *
     * @param id
     * @param nome
     * @param categoria
     * @param pomodoroRealizado
     * @param pomodoroPrevisto
     * @param deadline
     * @param concluida
     */
    public Atividade(int id, String nome, Categoria categoria, int pomodoroRealizado, int pomodoroPrevisto, Date deadline, int concluida) {
        this.id = id;
        this.nome = nome;
        this.categoria = categoria;
        this.pomodoroRealizado = pomodoroRealizado;
        this.pomodoroPrevisto = pomodoroPrevisto;
        this.deadline = deadline;
        this.concluida = concluida;
    }

    /**
     * Quando está criando pela primeira vez.
     *
     * @param nome
     * @param pomodoroRealizado
     * @param pomodoroPrevisto
     * @param deadline
     * @param categoria
     */
    public Atividade(String nome, int pomodoroRealizado, int pomodoroPrevisto, Date deadline, Categoria categoria) {
        this.nome = nome;
        this.pomodoroRealizado = pomodoroRealizado;
        this.pomodoroPrevisto = pomodoroPrevisto;
        this.deadline = deadline;
        this.categoria = categoria;
        this.concluida = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPomodoroRealizado() {
        return pomodoroRealizado;
    }

    public void setPomodoroRealizado(int pomodoroRealizado) {
        this.pomodoroRealizado = pomodoroRealizado;
    }

    public int getPomodoroPrevisto() {
        return pomodoroPrevisto;
    }

    public void setPomodoroPrevisto(int pomodoroPrevisto) {
        this.pomodoroPrevisto = pomodoroPrevisto;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public int getConcluida() {
        return concluida;
    }

    public void setConcluida(int concluida) {
        this.concluida = concluida;
    }

    public void validarPomodoro(){
        this.pomodoroRealizado++;
    }
}
