package developmentprocess.ifpb.jslsoft.com.pomostudy.persistencia;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Atividade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Categoria;

/**
 * Prover acesso a objetos Atividade
 *
 * Created by João on 18/02/2015.
 */
public class AtividadeDao implements IDao<Atividade, Integer> {

    private DatabaseHelper helper;
    private SQLiteDatabase db;
    private Activity context;


    public AtividadeDao(Activity context){
       this.context = context;
    }

    @Override
    public List<Atividade> buscaTodos() {
        abrirConexao();
        Cursor cursor = db.rawQuery("SELECT * FROM atividade", null);
        cursor.moveToFirst();
        List<Atividade> atividades = new ArrayList<>();
        Atividade atividade;
        Categoria categoria;
        Cursor cursorCategoria;
        for(int i = 0; i < cursor.getCount(); i++){
            cursorCategoria = db.rawQuery("SELECT * FROM categoria WHERE _id = " + cursor.getInt(5), null);
            cursorCategoria.moveToFirst();
            int id = cursorCategoria.getInt(0);
            String nome = cursorCategoria.getString(1);
            categoria = new Categoria(id, nome);
            atividade = new Atividade(cursor.getInt(0), cursor.getString(1), categoria, cursor.getInt(2), cursor.getInt(3), new Date(cursor.getLong(4)), cursor.getInt(6));
            atividades.add(atividade);
            cursor.moveToNext();
        }
        fecharConexao();
        return atividades;
    }

    @Override
    public Atividade buscaPorId(Integer key) {
        abrirConexao();

        Cursor cursor = db.rawQuery("SELECT * FROM atividade WHERE _id= " + key, null);
        cursor.moveToFirst();

        int id = cursor.getInt(0);
        String nome = cursor.getString(1);
        int pomodoroRealizado = cursor.getInt(2);
        int pomodoroPrevisto = cursor.getInt(3);
        Date deadline = new Date(cursor.getLong(4));
        int categoriaId = cursor.getInt(5);
        int concluida = cursor.getInt(6);

        CategoriaDao categoriaDao = new CategoriaDao(context);
        Categoria c = categoriaDao.buscaPorId(categoriaId);
        Atividade a = new Atividade(id,nome,c,pomodoroRealizado,pomodoroPrevisto,deadline, concluida);

        fecharConexao();

        return a;
    }

    @Override
    public void create(Atividade atividade) {
        abrirConexao();

        ContentValues atividadeValues = new ContentValues();
        atividadeValues.put("nome", atividade.getNome());
        atividadeValues.put("pomodoro_previsto", atividade.getPomodoroPrevisto());
        atividadeValues.put("deadline", atividade.getDeadline().getTime());
        atividadeValues.put("pomodoro_realizado", 0);
        atividadeValues.put("categoria_id", atividade.getCategoria().getId());
        atividadeValues.put("concluida", atividade.getConcluida());

        db.insert("atividade", null, atividadeValues);

        fecharConexao();
    }

    @Override
    public void update(Atividade atividade) {
        abrirConexao();
        ContentValues atividadeValues = new ContentValues();
        atividadeValues.put("nome", atividade.getNome());
        atividadeValues.put("pomodoro_previsto", atividade.getPomodoroPrevisto());
        atividadeValues.put("deadline", atividade.getDeadline().getTime());
        atividadeValues.put("pomodoro_realizado", atividade.getPomodoroRealizado());
        atividadeValues.put("categoria_id", atividade.getCategoria().getId());
        atividadeValues.put("concluida", atividade.getConcluida());
        db.update("atividade", atividadeValues, "_id = ?", new String[]{String.valueOf(atividade.getId())});
        fecharConexao();
    }

    @Override
    public void delete(Atividade atividade) {
        abrirConexao();
        db.execSQL("DELETE FROM atividade WHERE _id = " + atividade.getId());
        fecharConexao();
    }

    @Override
    public Atividade buscarPorNome(String nome) {
        abrirConexao();

        Cursor cursor = db.rawQuery("SELECT * FROM atividade WHERE nome = '" + nome+ "'", null);
        cursor.moveToFirst();

        int id = cursor.getInt(0);
        int pomodoroRealizado = cursor.getInt(2);
        int pomodoroPrevisto = cursor.getInt(3);
        Date deadline = new Date(cursor.getLong(4));
        int categoriaId = cursor.getInt(5);
        int concluida = cursor.getInt(6);

        cursor = db.rawQuery("SELECT * FROM categoria WHERE _id = " + categoriaId, null);
        cursor.moveToFirst();

        String categoriaNome = cursor.getString(1);

        Categoria c = new Categoria(id, categoriaNome);
        Atividade a = new Atividade(id,nome,c,pomodoroRealizado,pomodoroPrevisto,deadline, concluida);

        fecharConexao();

        return a;
    }

    public List<Atividade> buscarPorCategoria(Categoria categoria){
        abrirConexao();
        Cursor cursor = db.rawQuery("SELECT * FROM atividade WHERE categoria_id = " + categoria.getId(), null);
        cursor.moveToFirst();
        List<Atividade> atividades = new ArrayList<>();
        Atividade atividade;

        for(int i = 0; i < cursor.getCount(); i++){
            atividade = new Atividade(cursor.getInt(0), cursor.getString(1), categoria, cursor.getInt(2), cursor.getInt(3), new Date(cursor.getLong(4)), cursor.getInt(6));
            atividades.add(atividade);
            cursor.moveToNext();
        }
        fecharConexao();
        return atividades;
    }

    public void abrirConexao(){
        helper = new DatabaseHelper(context);
        db = helper.getWritableDatabase();
    }

    public void fecharConexao(){
        helper.close();
        db.close();
    }

    public List<Atividade> buscarPorStatus(boolean concluida) {
        String query = "";
        if(concluida){
            query = "SELECT * FROM atividade WHERE concluida = " + 1;
        }else{
            query = "SELECT * FROM atividade WHERE concluida = " + 0;
        }

        abrirConexao();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        List<Atividade> atividades = new ArrayList<>();
        Atividade atividade;
        Categoria categoria;
        Cursor cursorCategoria;
        for(int i = 0; i < cursor.getCount(); i++){
            cursorCategoria = db.rawQuery("SELECT * FROM categoria WHERE _id = " + cursor.getInt(5), null);
            cursorCategoria.moveToFirst();
            int id = cursorCategoria.getInt(0);
            String nome = cursorCategoria.getString(1);
            categoria = new Categoria(id, nome);
            atividade = new Atividade(cursor.getInt(0), cursor.getString(1), categoria, cursor.getInt(2), cursor.getInt(3), new Date(cursor.getLong(4)), cursor.getInt(6));
            atividades.add(atividade);
            cursor.moveToNext();
        }
        fecharConexao();
        return atividades;
    }
}
