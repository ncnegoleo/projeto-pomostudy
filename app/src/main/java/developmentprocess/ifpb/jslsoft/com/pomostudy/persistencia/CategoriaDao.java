package developmentprocess.ifpb.jslsoft.com.pomostudy.persistencia;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.CharArrayReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Atividade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Categoria;

/**
 * Prover acesso a objetos Categoria.
 *
 * Created by João on 19/02/2015.
 */
public class CategoriaDao implements IDao <Categoria, Integer> {

    private DatabaseHelper helper;
    private SQLiteDatabase db;
    private Activity context;

    public CategoriaDao(Activity context){
        this.context = context;
    }

    @Override
    public List<Categoria> buscaTodos() {
        abrirConexao();
        Cursor cursor = db.rawQuery("SELECT * FROM categoria", null);
        cursor.moveToFirst();
        List<Categoria> categorias = new ArrayList<>();
        Categoria categoria;
        for(int i = 0; i < cursor.getCount(); i++){
            categoria = new Categoria(cursor.getInt(0), cursor.getString(1));
            categorias.add(categoria);
            cursor.moveToNext();
        }
        fecharConexao();
        return categorias;
    }

    @Override
    public Categoria buscaPorId(Integer key) {
        abrirConexao();
        Cursor cursor = db.rawQuery("SELECT * FROM categoria WHERE _id = " + key, null);
        cursor.moveToFirst();

        String nome = cursor.getString(1);
        Categoria categoria = null;
        if(cursor.getCount() > 0)
            categoria = new Categoria(key, nome);

        fecharConexao();

        return categoria;
    }

    @Override
    public void create(Categoria categoria) {
        abrirConexao();
        ContentValues categoriaValues = new ContentValues();
        categoriaValues.put("nome", categoria.getNome());
        db.insert("categoria", null, categoriaValues);
        fecharConexao();
    }

    @Override
    public void update(Categoria categoria) {
        abrirConexao();
        ContentValues categoriaValues = new ContentValues();
        categoriaValues.put("nome", categoria.getNome());
        db.update("categoria", categoriaValues, "_id = ?", new String[]{String.valueOf(categoria.getId())});
        fecharConexao();
    }

    @Override
    public void delete(Categoria categoria) {
        abrirConexao();
        db.execSQL("DELETE FROM categoria WHERE _id = " + categoria.getId());
        fecharConexao();
    }

    @Override
    public Categoria buscarPorNome(String nome) {
        abrirConexao();
        Categoria categoria = null;
        Cursor cursor = db.rawQuery("SELECT * FROM categoria WHERE nome = '" + nome + "'", null);
        cursor.moveToFirst();
        if(cursor.getCount() > 0) {
            categoria = new Categoria(cursor.getInt(0), nome);
        }
        fecharConexao();
        return categoria;
    }

    public void abrirConexao(){
        helper = new DatabaseHelper(context);
        db = helper.getWritableDatabase();
    }

    public void fecharConexao(){
        helper.close();
        db.close();
    }
}
