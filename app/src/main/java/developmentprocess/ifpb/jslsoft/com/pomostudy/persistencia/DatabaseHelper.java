package developmentprocess.ifpb.jslsoft.com.pomostudy.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Classe reponsável por criar e atualizar o banco de dados do app.
 * Created by João on 14/02/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String BANCO_DADOS = "PomoStudy";
    private static int VERSAO = 1;

    public DatabaseHelper(Context context){
        super(context, BANCO_DADOS, null, VERSAO);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys = ON");
        //super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE categoria (_id INTEGER PRIMARY KEY, nome TEXT);");

        db.execSQL("CREATE TABLE atividade (_id INTEGER PRIMARY KEY," +
            "nome TEXT," +
            "pomodoro_realizado INTEGER," +
            "pomodoro_previsto INTEGER," +
            "deadline DATE," +
            "categoria_id INTEGER," +
            "concluida INTEGER, " +
            "FOREIGN KEY(categoria_id) REFERENCES categoria(_id) ON DELETE CASCADE);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
