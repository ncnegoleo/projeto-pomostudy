package developmentprocess.ifpb.jslsoft.com.pomostudy.persistencia;

import java.util.List;

/**
 * Define os comportamentos fundamentais de um provedor de acesso a objetos.
 *
 * Created by João on 18/02/2015.
 */
public interface IDao <T, PK> {

    /**
     * Busca todas as ocorrências do objeto T
     *
     * @return List de todos os objeto T persistidos no banco de dados.
     */
    public List<T> buscaTodos();

    /**
     * Busca um objeto específico no banco de dados a partir da chave primária.
     *
     * @param pk
     *            Chave de busca, identificador do objeto.
     * @return Objeto T
     */
    public T buscaPorId(PK pk);

    /**
     * Responsável por persistir o objeto no banco de dados.
     *
     * @param entidade
     *            Objeto que será persistido.
     */
    public void create(T entidade);

    /**
     * Atualiza as informações de uma entidade.
     *
     * @param entidade
     *            Objeto que será atualizado.
     */
    public void update(T entidade);

    /**
     * Remove a instância de determinado objeto do banco de dados.
     *
     * @param entidade
     *            Objeto que será removido do banco de dados.
     */
    public void delete(T entidade);

    public T buscarPorNome(String nome);
}
