package developmentprocess.ifpb.jslsoft.com.pomostudy.resource;

/**
 * Enum que configura as cores da aplicação.
 */
public enum Colors {

    DARKGREEN("#006d68"), WHITEGREEN("#ff55a385");

    private final String value;

    /**
     * Contrutor.
     * @param value valor da cor hexadecimal.
     */
    Colors(String value){
        this.value = value;
    }

    /**
     * Recupera a cor em hexadecimal.
     * @return cor em hexadecimal.
     */
    public String getValue(){
        return this.value;
    }

}
