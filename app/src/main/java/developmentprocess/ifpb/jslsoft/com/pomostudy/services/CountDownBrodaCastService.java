package developmentprocess.ifpb.jslsoft.com.pomostudy.services;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.net.URISyntaxException;

import developmentprocess.ifpb.jslsoft.com.pomostudy.R;
import developmentprocess.ifpb.jslsoft.com.pomostudy.activities.ActivityCronometro;
import developmentprocess.ifpb.jslsoft.com.pomostudy.config.MyNotifications;

/**
 * Esta classe serve para rodar o contador regressivo em modo de Service,
 * ou seja o contador não vai parar quando a aplicação for pausada. Ela é
 * responsável por enviar informações do contador para a activity do pomodoro
 * ({@link developmentprocess.ifpb.jslsoft.com.pomostudy.activities.ActivityCronometro})<p/>
 * e assim atualizando a interface do contador. <p/>
 * <p/>
 * {@value #COUNTDOWN_BR} registro do countdown para enviar infomações do contador por intent. <p/>
 * {@value #finished} indica se o contador acacou ou não, serve para atualizar o layout ao
 * resumir a aplicação e o contador ja tenha sido finalizado. <p/>
 * <p/>
 * @author leonardo.soares.ws@gamail.com <p/>
 * @see developmentprocess.ifpb.jslsoft.com.pomostudy.activities.ActivityCronometro
 */
public class CountDownBrodaCastService extends Service {

    public static boolean finished = false;
    public static final String COUNTDOWN_BR = "developmentprocess.ifpb.jslsoft" +
            ".com.pomostudy.services.countdown_br";

    private Context context = CountDownBrodaCastService.this;

    private Intent bi = new Intent(COUNTDOWN_BR);
    private CountDownTimer cdt = null;
    private long time = 0;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        cdt.cancel();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            // recupera o tempo total enviado do layout do pomodoro, para iniciar o contador.
            time = intent.getLongExtra(ActivityCronometro.TIMECOUNTDOWN, 1000);
        } catch (NullPointerException e) {
            // para service
            stopSelf();

        }

        // Cria uma intenção pendente que só é ativa quando for chamada por um método
        PendingIntent p = PendingIntent.getActivity(this, 0, new Intent(this, ActivityCronometro.class), 0);

        // Chama a notificação passando a intenteção pendente e as informações que aparecerão na notficação
        MyNotifications.callNotification(this, p, "Pomodoro rodando", "Pomostudy", "Pomodoro ativo...");

        cdt = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.i("CountDownBrodaCastService", "segundos restantes: " + millisUntilFinished / 1000);

                // Coloca as informações do contador para o intent.
                bi.putExtra("countdown", millisUntilFinished);

                // envia as informações
                sendBroadcast(bi);
            }

            @Override
            public void onFinish() {

                finished = true;

                // Toca o ringtone
                MyNotifications.playRingtone(context);

                // Chama novamente a tela de pomodoro
                MyNotifications.resumeScreen(context, ActivityCronometro.class);

                // Coloca as informações do estatus do contador no intent
                bi.putExtra("finish", true);

                // envia as infromações
                sendBroadcast(bi);

                Log.i("CountDownBrodaCastService", "Timer finished");
            }
        };

        cdt.start();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
