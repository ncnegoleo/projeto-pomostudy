package developmentprocess.ifpb.jslsoft.com.pomostudy.util;

import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.IPomoStudyFacade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Atividade;

/**
 * Created by João on 21/02/2015.
 */
public class AlterarStatusAtividadeComando implements IComando {

    private Atividade atividade;
    private IPomoStudyFacade fachada;

    public AlterarStatusAtividadeComando(Atividade atividade, IPomoStudyFacade fachada){
        this.atividade = atividade;
        this.fachada = fachada;
    }

    @Override
    public void execute() {
        if(atividade.getConcluida() == 0) {
            atividade.setConcluida(1);
        }else{
            atividade.setConcluida(0);
        }
        fachada.alterarAtividade(atividade);
    }
}
