package developmentprocess.ifpb.jslsoft.com.pomostudy.util;

import developmentprocess.ifpb.jslsoft.com.pomostudy.facade.IPomoStudyFacade;
import developmentprocess.ifpb.jslsoft.com.pomostudy.model.Atividade;

/**
 * Created by João on 21/02/2015.
 */
public class ValidarPomodoroComando implements IComando {

    private Atividade atividade;
    private IPomoStudyFacade fachada;

    public ValidarPomodoroComando(Atividade atividade, IPomoStudyFacade fachada){
        this.atividade = atividade;
        this.fachada = fachada;
    }

    @Override
    public void execute() {
        atividade.validarPomodoro();
        fachada.alterarAtividade(atividade);
    }
}
